import os
import re

import config


class Show(object):

    @classmethod
    def get_all(cls):
        shows = []
        for watch_folder in config.WATCH_FOLDERS:
            for item in os.listdir(watch_folder):
                if os.path.isdir(os.path.join(watch_folder, item)):
                    shows.append(cls(watch_folder, item))
        return shows

    @classmethod
    def get(cls, show_name):
        for show in cls.get_all():
            if show.name == show_name:
                return show

    def __init__(self, watch_folder, name):
        self._watch_folder = watch_folder
        self._name = name

    @property
    def name(self):
        return self._name

    @property
    def watch_folder(self):
        return self._watch_folder

    @property
    def seasons(self):
        return Season.get_all(self)

    def season(self, season_number):
        return Season.get(self, season_number)

    def __repr__(self):
        return "<Show '{}'>".format(self.name)


class Season(object):

    @classmethod
    def get_all(cls, show):
        seasons = []
        for season_folder in os.listdir(os.path.join(show.watch_folder,
                                                     show.name)):
            m = re.search('Season (\d*)', season_folder)
            if m:
                seasons.append(cls(show, season_folder, m.group(1)))
        return seasons

    @classmethod
    def get(cls, show, season_number):
        for season in cls.get_all(show):
            if season.number == season_number:
                return season

    def __init__(self, show, folder, number):
        self._show = show
        self._folder = folder
        self._number = int(number)

    @property
    def folder(self):
        return self._folder

    @property
    def number(self):
        return self._number

    @property
    def show(self):
        return self._show

    @property
    def episodes(self):
        return Episode.get_all(self)

    def episode(self, number):
        return Episode.get(self, number)

    def __repr__(self):
        return "<Season {} of {}>".format(self._number, self._show.name)


class Episode(object):

    @classmethod
    def get_all(cls, season):
        episodes = []
        for episode_file in os.listdir(os.path.join(season.show.watch_folder,
                                                    season.show.name,
                                                    season.folder)):
            m = re.search(".*S?(\d+)[Ex](\d+).*\.[avi|mp4]", episode_file)
            if m:
                episodes.append(cls(season, episode_file, m.group(2)))
        return episodes

    @classmethod
    def get(cls, season, number):
        for episode in cls.get_all(season):
            if episode.number == number:
                return episode

    def __init__(self, season, file, number):
        self._season = season
        self._file = file
        self._number = int(number)

    @property
    def season(self):
        return self._season

    @property
    def file(self):
        return self._file

    @property
    def number(self):
        return self._number

    def __repr__(self):
        return "<Episode S{}E{} of {}>".format(self.season.number,
                                               self.number,
                                               self.season.show.name)
