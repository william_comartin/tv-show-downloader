from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Show(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    path = Column(String(255))


class Season(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String(255))


class Episode(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String(255))


from sqlalchemy import create_engine
engine = create_engine('sqlite:///database.sqlite')

from sqlalchemy.orm import sessionmaker
session = sessionmaker()
session.configure(bind=engine)
