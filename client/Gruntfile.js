module.exports = function(grunt) {

  var pkgJson = require('./package.json');

  grunt.initConfig({

    pkg: pkgJson,


    'concat': {
      javascript: {
        src: [
          'src/javascript/application.js'
        ],
        dest: '.temp/application.concat.js',
      },
      stylesheets: {
        src: [
          '.temp/application.css'
        ],
        dest: '.temp/application.concat.css',
      },
      vendor_js: {
        src: pkgJson.bowerFiles.javascript,
        dest: '.temp/vendor.concat.js'
      },
      vendor_css: {
        src: pkgJson.bowerFiles.stylesheets,
        dest: '.temp/vendor.concat.css'
      }
    },

    'sync': {
      main: {
        files: [
          // includes files within path and its sub-directories
          {expand: true, cwd: 'src/images', src: ['**', '!**.pxm'], dest: 'dist/images'},
          {expand: true, cwd: 'src/fonts', src: ['**'], dest: 'dist/fonts'},
          {expand: true, cwd: 'src/templates', src: ['**.html'], dest: 'dist/templates'},
          {expand: true, cwd: 'src', src: ['**.*'], dest: 'dist'},

        ],
        updateOnly: true
      }
    },

    'cssmin': {
      application: {
        options: {
          banner: '/* Minified CSS - DO NOT MODIFY THIS FILE DIRECTLY */'
        },
        files: {
          '<%= pkg.development.local_path %>/css/app.min.css': ['.temp/application.concat.css'],
        }
      },
      vendor: {
        options: {
          banner: '/* Minified CSS - DO NOT MODIFY THIS FILE DIRECTLY */'
        },
        files: {
          '<%= pkg.development.local_path %>/css/vendor.min.css': ['.temp/vendor.concat.css']
        }
      }
    },

    'sass': {
      development: {
        options: {
          style: 'expanded'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss',
        }
      },
      staging: {
        options: {
          style: 'compressed'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss',
        }
      },
      production: {
        options: {
          style: 'compressed'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss',
        }
      }
    },

    'sftp-deploy': {
      development: {
        auth: {
          host: '<%= pkg.development.host %>',
          port: 22,
          authKey: 'development'
        },
        cache: '.temp/sftpCache-development.json',
        src: '<%= pkg.development.local_path %>',
        dest: '<%= pkg.development.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      },
      staging: {
        auth: {
          host: '<%= pkg.staging.host %>',
          port: 22,
          authKey: 'staging'
        },
        cache: '.temp/sftpCache-staging.json',
        src: '<%= pkg.staging.local_path %>',
        dest: '<%= pkg.staging.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      },
      production: {
        auth: {
          host: '<%= pkg.production.host %>',
          port: 22,
          authKey: 'production'
        },
        cache: '.temp/sftpCache-production.json',
        src: '<%= pkg.production.local_path %>',
        dest: '<%= pkg.production.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      }
    },

    'uglify': {
      development: {
        options: {
          beautify: true,
          mangle: false
        },
        files: {
          'dist/js/app.min.js': ['.temp/application.concat.js'],
          'dist/js/vendor.min.js': ['.temp/vendor.concat.js'],
        }
      },
      staging: {
        options: {
          beautify: false
        },
        files: {
          'dist/js/app.min.js': ['.temp/application.concat.js'],
          'dist/js/vendor.min.js': ['.temp/vendor.concat.js'],
        }
      },
      production: {
        options: {
          beautify: false
        },
        files: {
          'dist/js/app.min.js': ['.temp/application.concat.js'],
          'dist/js/vendor.min.js': ['.temp/vendor.concat.js'],
        }
      }
    },

    'watch': {
      scripts: {
        files: ['src/javascript/**'],
        tasks: ['concat:javascript',
                'concat:vendor_js',
                'uglify:development',
                'watch'],
        options: {
          spawn: false,
        }
      },
      stylesheets: {
        files: ['src/stylesheets/*.scss'],
        tasks: ['sass:development','concat:stylesheets','cssmin','watch'],
        options: {
          spawn: false,
        }
      },
      php: {
        files: ['src/**.*', 'src/images/**.*', 'src/templates/**.*'],
        tasks: ['sync','watch'],
        options: {
          spawn: false,
        }
      }
    },

    'connect': {
      server: {
        options: {
          livereload: true,
          port: 8000,
          base: 'dist'
        }
      }
    }

  });

  // Load Plugins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sftp-deploy');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-contrib-connect');

  // Default task creates a fresh build
  grunt.registerTask('default', ['build', 'connect:server', 'watch']);

  // grunt.registerTask('plugins', ['clean:plugins','curl-dir:plugins', 'unzip:plugins']);
  // grunt.registerTask('themes', ['clean:themes','curl-dir:themes', 'unzip:themes']);
  // grunt.registerTask('install', ['plugins', 'themes', 'build']);

  grunt.registerTask('build', ['concat:javascript',
                               'concat:vendor_js',
                               'uglify:development',
                               'sass:development',
                               'concat:stylesheets',
                               'concat:vendor_css',
                               'cssmin',
                               'sync']);

  grunt.registerTask('develop', ['build', 'sftp-deploy:development']);

  grunt.registerTask('stage', ['concat:javascript',
                               'concat:vendor_js',
                               'uglify:staging',
                               'sass:staging',
                               'concat:stylesheets',
                               'concat:vendor_css',
                               'cssmin',
                               'sync',
                               'sftp-deploy:staging']);
  grunt.registerTask('deploy', ['concat:javascript',
                                'concat:vendor_js',
                                'uglify:production',
                                'sass:production',
                                'concat:stylesheets',
                                'concat:vendor_css',
                                'cssmin',
                                'sync',
                                'sftp-deploy:production']);

};
